namespace RepoExample
{
    public interface IEntity
    {
        int ID { get; }
    }
}