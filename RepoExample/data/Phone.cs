namespace RepoExample
{
    public class Phone : IEntity
    {
        private int phoneId;
        public string Name;
        public bool IsChineese;
        public Phone()
        {
        }
        public Phone(int id, string name, bool isChineese)
        {
            phoneId = id;
            Name = name;
            IsChineese = isChineese;
        }

        public int ID => phoneId;
    }
}