using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace RepoExample
{
    public interface IRepository<T> where T : IEntity
    {
        void Insert(SqlCommand command);
        void Delete(SqlCommand command);
        IEnumerable<T> GetRecords(SqlCommand command);
        T GetRecord(SqlCommand command);
    }
}