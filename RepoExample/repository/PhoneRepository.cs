using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace RepoExample
{
    public class PhoneRepository : Repository<Phone>
    {
        public PhoneRepository(string connectionString) : base(connectionString)
        {
        }

        public IEnumerable<Phone> GetAll()
        {
            using (var command = new SqlCommand("SELECT * FROM phone"))
            {
                return GetRecords(command);
            }
        }

        public Phone GetById(int id)
        {
            using (var command = new SqlCommand("SELECT * FROM phone WHERE ID = @id"))
            {
                command.Parameters.Add("@id", SqlDbType.Int);
                command.Parameters["@id"].Value = id;
                return GetRecord(command);
            }
        }

        public void Add(Phone newOne)
        {
            using (var command = new SqlCommand("INSERT INTO phone(ID,name,is_chineese) VALUES(@param1,@param2,@param3)"))
            {
                command.Parameters.Add("@param1", SqlDbType.Int);
                command.Parameters["@param1"].Value = newOne.ID;
                command.Parameters.Add("@param2", SqlDbType.VarChar, 50);
                command.Parameters["@param2"].Value = newOne.Name;
                command.Parameters.Add("@param3", SqlDbType.Bit, 50);
                command.Parameters["@param3"].Value = newOne.IsChineese;
                command.CommandType = CommandType.Text;
                Insert(command);
            }
        }

        public override Phone PopulateRecord(SqlDataReader reader)
        {
            return new Phone(
                id: reader.GetInt32(0),
                name: reader.GetString(1),
                isChineese: reader.GetBoolean(2));
        }
    }
}