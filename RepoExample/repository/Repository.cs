using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace RepoExample
{
    public abstract class Repository<T> : IRepository<T> where T : IEntity
    {

        private static SqlConnection _connection;

        public virtual T PopulateRecord(SqlDataReader reader)
        {
            return default(T);
        }
        public Repository(string connectionString)
        {
            _connection = new SqlConnection(connectionString);
        }

        #region IRepository<T> Members
        public IEnumerable<T> GetRecords(SqlCommand command)
        {
            var list = new List<T>();
            command.Connection = _connection;
            _connection.Open();
            try
            {
                var reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                        list.Add(PopulateRecord(reader));
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
            }
            finally
            {
                _connection.Close();
            }
            return list;
        }
        public T GetRecord(SqlCommand command)
        {
            T record = default(T);
            command.Connection = _connection;
            _connection.Open();
            try
            {
                var reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        record = PopulateRecord(reader);
                        break;
                    }
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
            }
            finally
            {
                _connection.Close();
            }
            return record;
        }
        public void Delete(SqlCommand command)
        {
            throw new System.NotImplementedException();
        }

        public void Insert(SqlCommand command)
        {
            command.Connection = _connection;
            _connection.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                _connection.Close();
            }
        }
        #endregion
    }
}