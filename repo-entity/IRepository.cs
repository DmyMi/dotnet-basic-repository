using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace repo_entity
{
    public interface IRepository<T> where T : BaseEntity
    {
        IEnumerable<T> Get();
        IEnumerable<T> Get(Expression<Func<T, bool>> predicate);
        void Add(T entity);
        void Delete(T entity);
        void Update(T entity);
    }
}