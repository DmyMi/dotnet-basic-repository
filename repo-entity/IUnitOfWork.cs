using System;
using Microsoft.EntityFrameworkCore;

namespace repo_entity
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Phone> PhoneRepository { get; }
        void Commit();
    }
}