﻿using System;

namespace repo_entity
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var UoW = new UnitOfWork(new RozetkaContext()))
            {
                var phone = new Phone()
                {
                    Name = "Iphone X",
                    IsChineese = false
                };

                UoW.PhoneRepository.Add(phone);
                UoW.Commit();
            }
        }
    }
}
