using Microsoft.EntityFrameworkCore;

namespace repo_entity
{
    public class RozetkaContext : DbContext
    {
        public DbSet<Phone> Phones { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySQL("server=localhost;database=rozetka;user=user;password=password;protocol=tcp");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
        
    }
}