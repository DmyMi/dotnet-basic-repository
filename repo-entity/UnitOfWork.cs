using Microsoft.EntityFrameworkCore;

namespace repo_entity
{
    public class UnitOfWork :  IUnitOfWork
    {
        private DbContext context;
        private IRepository<Phone> phoneRepository;

        public UnitOfWork(DbContext context)
        {
            this.context = context;
        }

        public IRepository<Phone> PhoneRepository
        { 
            get
            {
                if (phoneRepository == null)
                {
                    phoneRepository = new Repository<Phone>(context);
                }
                return phoneRepository;
            } 
        }
        public void Commit()
        {
            context.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
            
        }
    }
}