using System.ComponentModel.DataAnnotations;

namespace repo_entity
{
    public class Phone : BaseEntity
    {
        [Required]
        [DataType(DataType.Text)]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        public bool IsChineese { get; set; }
    }
}